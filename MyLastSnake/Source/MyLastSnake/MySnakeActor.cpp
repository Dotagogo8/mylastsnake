// Fill out your copyright notice in the Description page of Project Settings.


#include "MySnakeActor.h"

// Sets default values
AMySnakeActor::AMySnakeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<USceneComponent>("MyRoot");
	//��������� ���� � ������
	RootComponent = MyRootComponent;
	// �������� ����� ��� ���������� �����
	FVector Posa = GetActorLocation();

	// ������ ������ ������ ����� ������������ �� �����
	MyRootComponent->SetRelativeLocation(Posa);

	// ������� ������� ��� ������
	CreateSnakeBody();
}

// Called when the game starts or when spawned
void AMySnakeActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMySnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetVisibleChank();

	BuferTime += DeltaTime;
	if (BuferTime > StepDelay)
	{

		MoveSnake();

		BuferTime = 0;
	}
}
// ������� �������� ���� ������
void AMySnakeActor::CreateSnakeBody()
{
	// ����� ����������� ������ UE ����� �� ������ ������� ����� ������� ���� ����
	class UStaticMesh* SnakeChankMesh;
	SnakeChankMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;
	// �������� ��� ������ 
	class UMaterialInstance* BodyColor;
	BodyColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterial/M_Metal_Steel_Inst.M_Metal_Steel_Inst'")).Get();
	// ������� �������� ��� ������ ������
	class UMaterialInstance* WormHead;
	WormHead = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterial/M_Tech_Hex_Tile_Pulse_Inst.M_Tech_Hex_Tile_Pulse_Inst'")).Get();
	
	// ���������� ��� ������� ��������� ������ 
	FVector NextPoint = GetActorLocation();

	// ��������� �������� ��� ����� ������ ���� ������ 
	FName NameChank;
	// ���������� ��� �������� ����� 
	FString TheString;

	// ������� ������� ������ ������� ���������� ��� ���� � ������ ������
	for (int32 i = 0; i < SnakeSize; i++)
	{
		// �������� ���������� ���
		TheString = "Chanks" + FString::FromInt(i);
		// ��������� ��� ���������� ��� � ����� TEXT("")
		NameChank = FName(*TheString);

		// ������� ����� ������
		class UStaticMeshComponent* BodyChank = CreateDefaultSubobject<UStaticMeshComponent>(NameChank);

		// ������ ������ ����� ������ ���� ����
		BodyChank->SetStaticMesh(SnakeChankMesh);
		// ������ ���������� ����� ���� ������������ ����� �������� ����� 
		BodyChank->SetRelativeLocation(NextPoint);
		/// ��������� ���� � ������
		SnakeBody.Add(BodyChank);
		// �������� �� ��� ��������� 
		NextPoint.X -= StepSnake;
		// ��������� � ������ ������ 
		BodyChank->AttachTo(MyRootComponent);
		// ������� ���� ����� ����
		BodyChank->SetMaterial(0, BodyColor);
		// ��������� ������ ����
		if (i == 0)
		{
			//������� ���� ������
			BodyChank->SetMaterial(0, WormHead);
			
			BodyChank->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			
			BodyChank->SetCollisionObjectType(ECC_WorldStatic);
			BodyChank->SetCollisionResponseToAllChannels(ECR_Overlap);
		}
		else
		{
			// ��������� ������� ������������ � �������
			BodyChank->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}
	}
	SetVisibleChank();
}

void AMySnakeActor::SetVisibleChank()
{
	// ���������� ��� ����� 
	for (int32 IndexShow = 0; IndexShow < SnakeBody.Num(); IndexShow++)
	{
		// ���� ������ ��� ������ � ������� �������� �� ��������� ���
		if (IndexShow < VisibleBodyChank)
		{
			SnakeBody[IndexShow]->SetVisibility(true, true); // ���������� �����
		}
		else
		{
			SnakeBody[IndexShow]->SetVisibility(false, true); // ������ �����
		}

	}
}

void AMySnakeActor::MoveSnake()
{
	// ���� �� � �������� �� ��������� ���� ��� �� ������ �� ������  ( ��� �����)
	if ((DirectionMoveSnake.X != 0) || (DirectionMoveSnake.Y != 0))
	{
		// ������� ����� ������ 
		for (int Chank = SnakeBody.Num() - 1; Chank > 0; Chank--)
		{
			// �������� ������� ���������� ����� ������ 
			FVector V = SnakeBody[Chank - 1]->GetRelativeLocation();
			// ������ ����� ������� ��� ������ 
			SnakeBody[Chank]->SetRelativeLocation(V);
		}

			// �������� ����� 1 ����� 
			FVector StartPoint = SnakeBody[0]->GetRelativeLocation();
			// ���� ����������� �� � >0 �� ������� ���� � ���� �������
			if (DirectionMoveSnake.X > 0) StartPoint.X -= StepSnake;
			if (DirectionMoveSnake.X < 0) StartPoint.X += StepSnake;
			// � ������ �������
			if (DirectionMoveSnake.Y > 0) StartPoint.Y += StepSnake;
			if (DirectionMoveSnake.Y < 0) StartPoint.Y -= StepSnake;
			// ������ ������ � ����� �������
			SnakeBody[0]->SetRelativeLocation(StartPoint);
		
	}
};

