// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyLastSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYLASTSNAKE_API AMyLastSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
