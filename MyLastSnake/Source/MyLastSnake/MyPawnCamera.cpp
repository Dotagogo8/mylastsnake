// Fill out your copyright notice in the Description page of Project Settings.

#include "MySnakeActor.h"
#include "MyPawnCamera.h"
#include "Components/InputComponent.h"
// ��������� ����� ��������� �������
#include "MySnakeActor.h"
#include "MyFoodActor.h"

// Sets default values
AMyPawnCamera::AMyPawnCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ������� �������� �������� ��� ������
	MySceneComponent = CreateDefaultSubobject<USceneComponent>("RootModel");
	// ��������� ���� � �����
	RootComponent = MySceneComponent;
	// ������� ������ ��� ������
	MyCameraSpring = CreateDefaultSubobject<USpringArmComponent>("Spring");
	// ��������� ������� ������� 
	MyCameraSpring->SetRelativeLocation(FVector(0, 0, 0));
	// ����������� � ������
	MyCameraSpring->AttachTo(MySceneComponent);

	// create camera
	MyCamera = CreateDefaultSubobject<UCameraComponent>("camera");
	// attach camera to tripod(������)
	MyCamera->AttachTo(MyCameraSpring, USpringArmComponent::SocketName);

	// ������������� ��������� ( ������� �������)
	MyCameraSpring->SetRelativeRotation(FRotator(-90.f, 0, 0));
	// ��������� ����� �������
	MyCameraSpring->TargetArmLength = 1700.f;
	// ���� �������� �� �������� � �������
	MyCameraSpring->bDoCollisionTest = false;

	//������������� ����������  �� 0 ������ 
	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void AMyPawnCamera::BeginPlay()
{
	Super::BeginPlay();
	///AddSnakeToMap();
	
}

// Called every frame
void AMyPawnCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// ����� ����� �������� ���
	BuferTime += DeltaTime;
	if (BuferTime > StepDelay)
	{
       if (!GamePause) AddRandomEat();
	   BuferTime = 0;
	}
	

}

// Called to bind functionality to input
void AMyPawnCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//�������� ��������� �� ������ WSAD
	PlayerInputComponent->BindAxis("KeyMapMove", this, &AMyPawnCamera::FMove);

}

void AMyPawnCamera::AddSnakeToMap()
{
	//����� ����� ����
	FVector StartPoint = GetActorLocation();

	FRotator StartPointRotation = GetActorRotation();

	if (GetWorld())
	{
		MySnakePlayer = GetWorld()->SpawnActor<AMySnakeActor>(StartPoint, StartPointRotation);
		GameMode = 1;
	}
}

void AMyPawnCamera::FMove(float ButtonVal)
{
	int32 Key = ButtonVal;

	if (Key == 5)
	{
		GamePause = !GamePause;
	}

	if (!GamePause) {

		// ��������� ��� ����� �����
		switch (Key)
		{
			// W
		case 1:
			if (WSAD.X != 1)
			{
				WSAD = FVector2D(0, 0);
				WSAD.X = -1;
			}
			break;

			// S
		case 2:
			if (WSAD.X != -1)
			{
				WSAD = FVector2D(0, 0);
				WSAD.X = 1;
			}
			break;

			// A
		case 3:
			if (WSAD.Y != -1)
			{
				WSAD = FVector2D(0, 0);
				WSAD.Y = 1;
			}
			break;

			// D
		case 4:
			if (WSAD.Y != 1)
			{
				WSAD = FVector2D(0, 0);
				WSAD.Y = -1;
			}
			break;
		}

		//��������� ������� ������ �� ������
		if (MySnakePlayer)
		{
			MySnakePlayer->DirectionMoveSnake = WSAD;
		}
	}
	else 
	{
		MySnakePlayer->DirectionMoveSnake = FVector2D(0, 0);
	}
}

void AMyPawnCamera::AddRandomEat()
{
	// ���� �������� ��� �������� ���
	FRotator StartPointRotation = FRotator(0, 0, 0);

	// �������� ��������� ����� �� X ��� �������� ���
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	// �������� ��������� ����� �� Y ��� �������� ���
	float SpawnY = FMath::FRandRange(MinY, MaxY);

	//  ����� ��� ���������� ����� � ���� ����
	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	// ��������� ������� ������( ���� ���� �� ������� ���)
	if (MySnakePlayer)
	{
		// ��������� ������� ����� ���� 
		if (GetWorld())
		{
			GetWorld()->SpawnActor<AMyFoodActor>(StartPoint, StartPointRotation);
		}
	}
}

// �������� ������� (����)
int32 AMyPawnCamera::GetScore()
{
	if (MySnakePlayer)
	{
		return MySnakePlayer->score;
	}
	return 0;
}