// Fill out your copyright notice in the Description page of Project Settings.


#include "MyLastSnake.h"
#include "MyFoodActor.h"
#include "MySnakeActor.h"

// Sets default values
AMyFoodActor::AMyFoodActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ������� �������� �������� ���� ������
	MyRootComponent = CreateDefaultSubobject<USceneComponent>("RootEat");
	// ��������� ���� � ������
	RootComponent = MyRootComponent;

	// ����� ����������� ������ 
	SnakeEatMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	// �������� ��� ���
	class UMaterialInstance* EatColor;
	// ��������� �������� ��� ����� ���
	EatColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterial/M_Metal_Rust_Inst.M_Metal_Rust_Inst'")).Get();

	// ������ ���
	FVector Size = FVector(0.5f, 0.5f, 0.5f);

	// ������� ����������� ���� ��� �������� ����� � ������
	class UStaticMeshComponent* EatChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Eat"));

	// ������ ����������� ����� ��� �����
	EatChank->SetStaticMesh(SnakeEatMesh);
	// ������ ����� ������ ��� ������ 
	EatChank->SetRelativeScale3D(Size);
	// ������ ������� ��� ���
	EatChank->SetRelativeLocation(FVector(0, 0, 0));
	// ������ �������� ��� ����� ���
	EatChank->SetMaterial(0, EatColor);
	// ������� ����
	EatChank->AttachTo(MyRootComponent);

	// ��� �������� ������ ��� ���
	EatChank->SetSimulatePhysics(true);

}

// Called when the game starts or when spawned
void AMyFoodActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyFoodActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// ������������ ���-�� ����������� ������� 
	BuferTime += DeltaTime;

	if (BuferTime > StepDelay)
	{
		//���������� ������(�������)
		Destroy(true, true);
	}

	CollectEat();
}

// ������� ����� ���
void AMyFoodActor::CollectEat()	
{
	// ������� ������ ���� ����� �������� ��� ������������
	TArray<AActor*> CollectedActors;
	//��������� ���� ������� � �������� �����������
	GetOverlappingActors(CollectedActors);

	//��������� ���� � ��� ���� ������������
	for (int32 i = 0; i < CollectedActors.Num(); ++i)
	{
		AMySnakeActor* const Test = Cast<AMySnakeActor>(CollectedActors[i]);

		if (Test)
		{
			Test->VisibleBodyChank++;

			Test->score++;


			// ���������� ��� , ��� ��������� ������
			Destroy(true, true);
			break; // ������������� ����
		}
	}
}

