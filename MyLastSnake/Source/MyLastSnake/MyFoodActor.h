// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyFoodActor.generated.h"

UCLASS()
class MYLASTSNAKE_API AMyFoodActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyFoodActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// ��������� ����� ������� ����� ������ ���� �������� ��� ���
	class USceneComponent* MyRootComponent;

	// �������� ��� ���
	class UStaticMesh* SnakeEatMesh;

	//������� ����� ���
	void CollectEat();

	///UPROPERTY(EditAnywhere) TArray<AActor*> CollectedActors;

	// �������� �� ������ 
	float StepDelay = 8.0f;
	// ���������� �������
	float BuferTime = 0;
};
