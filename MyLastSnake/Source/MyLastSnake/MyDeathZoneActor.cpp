// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDeathZoneActor.h"

// Sets default values
AMyDeathZoneActor::AMyDeathZoneActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ������� ���� ������ �� ������������ ����
	class UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;

	// ��������� ��������
	WallColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterial/Danger_Inst.Danger_Inst'")).Get();

	// ������� �������� �������� ��� ������
	MyRootComponent = CreateDefaultSubobject<USceneComponent>("RootModel");
	// ��������� ���� � ������
	RootComponent = MyRootComponent;

	class UStaticMeshComponent* WallChank;
	// ������� ������ ���� ��� �������� ����� � ������
	WallChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	// ������� ������ ����� ��� �����
	WallChank->SetStaticMesh(WallMesh);
	// ������ ������� 
	WallChank->SetRelativeLocation(FVector(0, 0, 0));
	// ������ ����������� ��������
	WallChank->SetMaterial(0, WallColor);

	// ������� ����
	WallChank->AttachTo(MyRootComponent);

}

// Called when the game starts or when spawned
void AMyDeathZoneActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyDeathZoneActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CollideWall();

}

void AMyDeathZoneActor::CollideWall()
{
	//������� ������ ��� ������ ������������
	TArray<AActor*>(CollectedActors);
	
	// ��������� ���� ������ � �������� ����������� 
	GetOverlappingActors(CollectedActors);

	for (int32 i = 0; i < CollectedActors.Num(); ++i)
	{
		// ���������� ��� ��� ���������� � �����
		CollectedActors[i]->Destroy(true, true);
	}
}

