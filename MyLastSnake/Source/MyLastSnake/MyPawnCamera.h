// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "MyPawnCamera.generated.h"

UCLASS()
class MYLASTSNAKE_API AMyPawnCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawnCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// �������� ���� ��� ������ ������
	UPROPERTY(EditAnywhere)
		USceneComponent* MySceneComponent;

	// ������ ��� ������
	UPROPERTY(EditAnywhere)
		USpringArmComponent* MyCameraSpring;

	// ������ ���������
	UPROPERTY(EditAnywhere)
		UCameraComponent* MyCamera;

	//��������� �����
	class AMySnakeActor* MySnakePlayer;

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	// ��������� ����� �� ������
	void AddSnakeToMap();

	
	void FMove(float ButtonVal);

	// ���������� �������� ������
	FVector2D WSAD = (0, 0);

	// �������� �������� ��� �� �����
	float MinY = -1150.f;   float MaxY = 1150.f;
	float MinX = -650.f;    float MaxX = 650.f;

	// ������ � ������� ����� ������ ����� ��� ( �� Z)
	float SpawnZ = 50.f;

	// ������� ������� �������� ��� � �������� ������ �����
	void AddRandomEat();

	// �������� ��� ���
	float StepDelay = 1.f;
	// ���������� ������� 
	float BuferTime = 0;

	// ���������� ������ ���� 
	int32 GameMode = 0;
	// ������ ������� ������� ����� ������ ������ ���� �� ���������������
	UFUNCTION(BluePrintCallable, Category = "SnakePawn")
		int32 GetGameMode() const { return GameMode; }

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		int32 GetScore();

	// ���������� ��� ������� ����� � ����
	bool GamePause = false;

	// ������� ����� ���������� ������ ����� � �������� ����� 
	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		bool GetGamePause() const { return GamePause; }

};
