// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYLASTSNAKE_MyFoodActor_generated_h
#error "MyFoodActor.generated.h already included, missing '#pragma once' in MyFoodActor.h"
#endif
#define MYLASTSNAKE_MyFoodActor_generated_h

#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_SPARSE_DATA
#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_RPC_WRAPPERS
#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyFoodActor(); \
	friend struct Z_Construct_UClass_AMyFoodActor_Statics; \
public: \
	DECLARE_CLASS(AMyFoodActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyLastSnake"), NO_API) \
	DECLARE_SERIALIZER(AMyFoodActor)


#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyFoodActor(); \
	friend struct Z_Construct_UClass_AMyFoodActor_Statics; \
public: \
	DECLARE_CLASS(AMyFoodActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyLastSnake"), NO_API) \
	DECLARE_SERIALIZER(AMyFoodActor)


#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyFoodActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyFoodActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyFoodActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyFoodActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyFoodActor(AMyFoodActor&&); \
	NO_API AMyFoodActor(const AMyFoodActor&); \
public:


#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyFoodActor(AMyFoodActor&&); \
	NO_API AMyFoodActor(const AMyFoodActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyFoodActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyFoodActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyFoodActor)


#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_PRIVATE_PROPERTY_OFFSET
#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_9_PROLOG
#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_PRIVATE_PROPERTY_OFFSET \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_SPARSE_DATA \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_RPC_WRAPPERS \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_INCLASS \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_PRIVATE_PROPERTY_OFFSET \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_SPARSE_DATA \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_INCLASS_NO_PURE_DECLS \
	MyLastSnake_Source_MyLastSnake_MyFoodActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYLASTSNAKE_API UClass* StaticClass<class AMyFoodActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyLastSnake_Source_MyLastSnake_MyFoodActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
