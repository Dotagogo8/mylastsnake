// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyLastSnake/MyDeathZoneActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyDeathZoneActor() {}
// Cross Module References
	MYLASTSNAKE_API UClass* Z_Construct_UClass_AMyDeathZoneActor_NoRegister();
	MYLASTSNAKE_API UClass* Z_Construct_UClass_AMyDeathZoneActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MyLastSnake();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstance_NoRegister();
// End Cross Module References
	void AMyDeathZoneActor::StaticRegisterNativesAMyDeathZoneActor()
	{
	}
	UClass* Z_Construct_UClass_AMyDeathZoneActor_NoRegister()
	{
		return AMyDeathZoneActor::StaticClass();
	}
	struct Z_Construct_UClass_AMyDeathZoneActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MyRootComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MyRootComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WallColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WallColor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyDeathZoneActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MyLastSnake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDeathZoneActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MyDeathZoneActor.h" },
		{ "ModuleRelativePath", "MyDeathZoneActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_MyRootComponent_MetaData[] = {
		{ "Category", "MyDeathZoneActor" },
		{ "Comment", "// ????????? ???? ??? ??????????? ????\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MyDeathZoneActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_MyRootComponent = { "MyRootComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyDeathZoneActor, MyRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_MyRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_MyRootComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_WallColor_MetaData[] = {
		{ "Category", "MyDeathZoneActor" },
		{ "Comment", "// ???????? ??? ?????\n" },
		{ "ModuleRelativePath", "MyDeathZoneActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_WallColor = { "WallColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyDeathZoneActor, WallColor), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_WallColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_WallColor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyDeathZoneActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_MyRootComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyDeathZoneActor_Statics::NewProp_WallColor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyDeathZoneActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyDeathZoneActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyDeathZoneActor_Statics::ClassParams = {
		&AMyDeathZoneActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMyDeathZoneActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyDeathZoneActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyDeathZoneActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDeathZoneActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyDeathZoneActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyDeathZoneActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyDeathZoneActor, 182904099);
	template<> MYLASTSNAKE_API UClass* StaticClass<AMyDeathZoneActor>()
	{
		return AMyDeathZoneActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyDeathZoneActor(Z_Construct_UClass_AMyDeathZoneActor, &AMyDeathZoneActor::StaticClass, TEXT("/Script/MyLastSnake"), TEXT("AMyDeathZoneActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyDeathZoneActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
