// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYLASTSNAKE_MyLastSnakeGameModeBase_generated_h
#error "MyLastSnakeGameModeBase.generated.h already included, missing '#pragma once' in MyLastSnakeGameModeBase.h"
#endif
#define MYLASTSNAKE_MyLastSnakeGameModeBase_generated_h

#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_SPARSE_DATA
#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_RPC_WRAPPERS
#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyLastSnakeGameModeBase(); \
	friend struct Z_Construct_UClass_AMyLastSnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyLastSnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyLastSnake"), NO_API) \
	DECLARE_SERIALIZER(AMyLastSnakeGameModeBase)


#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyLastSnakeGameModeBase(); \
	friend struct Z_Construct_UClass_AMyLastSnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyLastSnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyLastSnake"), NO_API) \
	DECLARE_SERIALIZER(AMyLastSnakeGameModeBase)


#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyLastSnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyLastSnakeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyLastSnakeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyLastSnakeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyLastSnakeGameModeBase(AMyLastSnakeGameModeBase&&); \
	NO_API AMyLastSnakeGameModeBase(const AMyLastSnakeGameModeBase&); \
public:


#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyLastSnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyLastSnakeGameModeBase(AMyLastSnakeGameModeBase&&); \
	NO_API AMyLastSnakeGameModeBase(const AMyLastSnakeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyLastSnakeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyLastSnakeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyLastSnakeGameModeBase)


#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_12_PROLOG
#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_SPARSE_DATA \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_RPC_WRAPPERS \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_INCLASS \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_SPARSE_DATA \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYLASTSNAKE_API UClass* StaticClass<class AMyLastSnakeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyLastSnake_Source_MyLastSnake_MyLastSnakeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
