// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyLastSnake/MySnakeActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMySnakeActor() {}
// Cross Module References
	MYLASTSNAKE_API UClass* Z_Construct_UClass_AMySnakeActor_NoRegister();
	MYLASTSNAKE_API UClass* Z_Construct_UClass_AMySnakeActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MyLastSnake();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	void AMySnakeActor::StaticRegisterNativesAMySnakeActor()
	{
	}
	UClass* Z_Construct_UClass_AMySnakeActor_NoRegister()
	{
		return AMySnakeActor::StaticClass();
	}
	struct Z_Construct_UClass_AMySnakeActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MovementSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisibleBodyChank_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_VisibleBodyChank;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectionMoveSnake_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DirectionMoveSnake;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMySnakeActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MyLastSnake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySnakeActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MySnakeActor.h" },
		{ "ModuleRelativePath", "MySnakeActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySnakeActor_Statics::NewProp_MovementSpeed_MetaData[] = {
		{ "Category", "MySnakeActor" },
		{ "Comment", "// ???????? ????????\n" },
		{ "ModuleRelativePath", "MySnakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMySnakeActor_Statics::NewProp_MovementSpeed = { "MovementSpeed", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMySnakeActor, MovementSpeed), METADATA_PARAMS(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_MovementSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_MovementSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleBodyChank_MetaData[] = {
		{ "Category", "MySnakeActor" },
		{ "Comment", "// ?????? ???-?? ??????? ?????? ?????? \n" },
		{ "ModuleRelativePath", "MySnakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleBodyChank = { "VisibleBodyChank", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMySnakeActor, VisibleBodyChank), METADATA_PARAMS(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleBodyChank_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleBodyChank_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake_MetaData[] = {
		{ "Category", "MySnakeActor" },
		{ "ModuleRelativePath", "MySnakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake = { "DirectionMoveSnake", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMySnakeActor, DirectionMoveSnake), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMySnakeActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySnakeActor_Statics::NewProp_MovementSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleBodyChank,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMySnakeActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMySnakeActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMySnakeActor_Statics::ClassParams = {
		&AMySnakeActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMySnakeActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMySnakeActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMySnakeActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMySnakeActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMySnakeActor, 2879777307);
	template<> MYLASTSNAKE_API UClass* StaticClass<AMySnakeActor>()
	{
		return AMySnakeActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMySnakeActor(Z_Construct_UClass_AMySnakeActor, &AMySnakeActor::StaticClass, TEXT("/Script/MyLastSnake"), TEXT("AMySnakeActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMySnakeActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
